"""Tests for the djikstra module."""

import math

import pytest
from djikstra import DjikstraSolver, Edge, Graph, MinPriorityQueue


@pytest.fixture
def graph():
    """Return a simple test graph."""
    return Graph(
        [
            Edge("a", "b", 4),
            Edge("a", "c", 2),
            Edge("b", "c", 3),
            Edge("b", "d", 2),
            Edge("b", "e", 3),
            Edge("c", "b", 1),
            Edge("c", "d", 4),
            Edge("c", "e", 5),
            Edge("e", "d", 1),
        ],
    )


class TestGraph(object):
    """Test the Graph class."""

    def test_init(self, graph):
        """Test that the vertices are correct."""
        assert graph.vertices == {"a", "b", "c", "d", "e"}

    def test_neighbors(self, graph):
        """Test that the neighbors are correct."""
        assert graph.neighbors("a") == {"b", "c"}
        assert graph.neighbors("") == set()
        assert graph.neighbors("e") == {"d"}

    def test_distance(self, graph):
        """Test that the distances are correctly determined."""
        assert graph.distance("a", "b") == 4
        assert graph.distance("e", "d") == 1

        with pytest.raises(ValueError) as exc:
            graph.distance("s", "d")
        assert str(exc.value) == "No edge found for s -> d"  # noqa: WPS441

    def test_add_edge(self, graph):
        """Test that the vertices are correct."""
        graph.add_edge(Edge("a", "x", 2))
        assert graph.vertices == {"a", "b", "c", "d", "e", "x"}
        assert graph.neighbors("a") == {"b", "c", "x"}
        assert graph.distance("a", "x") == 2

    def test_equals(self, graph):
        """Test __eq__ on the graph."""
        assert graph == graph  # noqa: WPS312
        assert graph != Graph([Edge("a", "b", 2)])


def test_priority_queue():
    """Test the priority queue."""
    queue = MinPriorityQueue({"a": 0, "b": math.inf, "c": math.inf})

    assert queue.extract_min() == "a"
    assert not queue.is_empty()

    queue.set_priority("c", 2)
    queue.set_priority("b", 5)

    assert queue.extract_min() == "c"
    assert queue.extract_min() == "b"
    assert queue.is_empty()


@pytest.fixture
def solver(graph):
    """Return a Djikstra solver for the sample graph."""
    return DjikstraSolver(graph, "a")


class TestDjikstraSolver(object):

    def test_distance(self, solver):
        """Test the distances of the shortest path."""
        assert solver.distance("a") == 0
        assert solver.distance("b") == 3
        assert solver.distance("c") == 2
        assert solver.distance("d") == 5
        assert solver.distance("e") == 6

    def test_paths(self, solver):
        """Test the shortest paths to all targets."""
        assert solver.shortest_path_to("a") == ["a"]
        assert solver.shortest_path_to("b") == ["a", "c", "b"]
        assert solver.shortest_path_to("c") == ["a", "c"]
        assert solver.shortest_path_to("d") == ["a", "c", "b", "d"]
        assert solver.shortest_path_to("e") == ["a", "c", "b", "e"]

    def test_subgraph(self, solver):
        """Verify the generated Subgraph."""
        expected = Graph(
            [
                Edge("a", "c", 2),
                Edge("b", "d", 2),
                Edge("b", "e", 3),
                Edge("c", "b", 1),
            ],
        )

        assert solver.shortest_paths() == expected
