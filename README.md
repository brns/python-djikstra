# Djikstra Algorithm Solver


## About

Solver for the Djikstra shortest path Algorithm written in Python.

It uses an implementation of the priority queue version as described on
[Wikipedia](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Using_a_priority_queue).

The test case is based on the following [example on Youtube](https://www.youtube.com/watch?v=_lHSawdgXpI).


## Setup

    pip install -r requirements.txt


## Test

    pytest


## Lint

    make lint


## Format

    make format


## Usage

Please refer to the test file for a complete example.

