format:
	@echo "----------------------------"
	@echo Reformat the code
	@echo "----------------------------"
	isort .
	black .

test:
	@echo "----------------------------"
	@echo Run tests and checks
	@echo "----------------------------"
	pytest

lint:
	@echo "----------------------------"
	@echo Run typecheck and linter
	@echo "----------------------------"
	mypy .
	flake8 .


.PHONY:
	format test lint
