"""Solver for Djikstra Shortest Path Algorithm."""

import math
from typing import Dict, Iterable, List, NamedTuple, Optional, Set, Union


class Edge(NamedTuple):
    """Weighted edge from source to destination."""

    source: str
    destination: str
    weight: int


class Graph(object):
    """A weighted graph."""

    edges: Set[Edge]
    vertices: Set[str]

    def __init__(self, edges: Iterable[Edge]):
        """Create a graph with the given edges."""
        self.edges = set(edges)
        self.vertices = {
            node for edge in edges for node in (edge.source, edge.destination)
        }

    def __repr__(self):  # pragma: no cover
        return "Graph(edges={edges})".format(edges=self.edges)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Graph):  # pragma: no cover
            return NotImplemented
        return self.edges == other.edges

    def add_edge(self, edge: Edge) -> None:
        """Add the edge if no similar edge is already defined."""
        self.edges.add(edge)
        self.vertices.update({edge.source, edge.destination})

    def neighbors(self, vertex: str) -> Set[str]:
        """Get all vertices that are reachable from the source."""
        return {edge.destination for edge in self.edges if edge.source == vertex}

    def distance(self, source: str, destination: str) -> int:
        """Get the distance from source to destination.

        :raises ValueError: if none or multiple edges match
        """
        edges = [
            edge
            for edge in self.edges
            if edge.source == source and edge.destination == destination
        ]

        if not edges:
            raise ValueError(
                "No edge found for {src} -> {dst}".format(src=source, dst=destination),
            )

        edge = min(edges, key=lambda edge: edge.weight)
        return edge.weight


class MinPriorityQueue(object):
    """Acts as queue that returns the element with the smallest priority."""

    elements: Dict[str, Union[int, float]]

    def __init__(self, elements: Dict[str, Union[int, float]]):
        """Initialise and validate the elements."""
        self.elements = dict(elements)

    def set_priority(self, name: str, priority: Union[int, float]) -> None:
        """Set the priority for an element.

        :raises ValueError: when the priority is negative
        """
        self.elements[name] = priority

    def extract_min(self) -> str:
        """Return the element with the smallest priority."""
        element = min(
            self.elements, key=lambda el: self.elements.get(el, 0)
        )  # noqa: E731
        self.elements.pop(element)
        return element

    def is_empty(self) -> bool:
        """Check if the queue is empty."""
        return not self.elements


class DjikstraResult(NamedTuple):
    """Result of solving the Djikstra Algorithm."""

    distances: Dict[str, Union[int, float]]
    predecessors: Dict[str, str]


def run_djikstra(graph: Graph, source: str) -> DjikstraResult:  # noqa: WPS210
    """
    Djikstra Shortest Path Algorithm.

    Implementation is adapted from
    https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Using_a_priority_queue

    Returns for each vertex the distances to the source node and
    the predecessor in the shortest path.
    """
    previous = {}

    distances = {vtx: math.inf for vtx in graph.vertices if vtx != source}
    distances[source] = 0

    queue = MinPriorityQueue(distances)

    while not queue.is_empty():
        current = queue.extract_min()

        # Check all neighbors
        for destination in graph.neighbors(current):
            # Get the distance from the source via the current node
            dist = distances[current] + graph.distance(current, destination)
            # If it is smaller than before, set the current
            # node as predecessor for the shortest path.
            if dist < distances[destination]:
                distances[destination] = dist
                previous[destination] = current

                # Decrease the priority
                queue.set_priority(destination, dist)

    return DjikstraResult(distances, previous)


class DjikstraSolver(object):
    """
    Solver for the Djikstra Algorithm.

    Derives distance, path or the subgraph from the solution.
    """

    graph: Graph
    source: str

    solution: DjikstraResult

    def __init__(self, graph: Graph, source: str):
        """Set the solver to get the shortest paths from the source."""
        self.graph = graph
        self.source = source

        self.solution = run_djikstra(self.graph, self.source)

    def distance(self, destination: str) -> Optional[Union[int, float]]:
        """Get the distance of the shortest path to the destination."""
        return self.solution.distances.get(destination)

    def shortest_path_to(self, destination: str) -> List[str]:
        """Get the shortest path to the destination."""
        path = [destination]

        # Recur over predecessors until the source is reached
        prev = self.solution.predecessors.get(destination)
        while prev:
            path.append(prev)
            prev = self.solution.predecessors.get(prev)

        path.reverse()
        return path

    def shortest_paths(self):
        """Return the Subgraph with only the shortest paths left."""
        subgraph = Graph([])

        for destination in self.graph.vertices:
            path = self.shortest_path_to(destination)

            if len(path) < 2:
                continue

            # iterate over the path and push
            # all edges
            src = path.pop(0)
            while len(path):
                dst = path.pop(0)
                subgraph.add_edge(Edge(src, dst, self.graph.distance(src, dst)))
                src = dst

        return subgraph
